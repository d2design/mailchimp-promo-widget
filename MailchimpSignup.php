<?php
    /*
    Plugin Name: Mailchimp Signup
    Plugin URI:
    Description: Mailchimp Sign-up widget plugin for page header
    Author: David Diomede
    Version: 1.0
    Author URI: http://d2creativedesign.com/
    */


//register MailchimpSignup

    class MailchimpSignup extends WP_Widget
    {
        // class constructor
        public function __construct()
        {
            $widget_ops = [
                'classname' => 'MailchimpSignup',
                'descriotion' => 'Mailchimp Sign-up widget plugin for page header'
            ];
            parent::__construct( 'MailchimpSignup', 'Mailchimp Signup', $widget_ops );
        }

        public function form( $instance )
        {
            $title = !empty( $instance[ 'title' ] ) ? $instance[ 'title' ] : esc_html__( 'Title', 'text_domain' );
            ?>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
                    <?php esc_attr_e( 'Title:'); ?>
                </label>

                <input
                        class="widefat"
                        id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                        name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                        type="text"
                        value="<?php echo esc_attr( $title ); ?>">
            </p>
            <?php
        }

        // output the widget content on the front-end
        public function widget( $args, $instance )
        {
            $args = [
                    'before_widget'=> '<div class="promo-header pt-2 pb-2"><div class="container"><div class="row">',
                    'before_title' => '<h5 class="align-middle">',
                    'after_title' => '</h5>',
                    'after_widget' => '</div></div></div>',
            ];

            echo $args[ 'before_widget' ];
            ?>
            <div id="mc_embed_signup">
                <form action="https://thecraftrevolt.us17.list-manage.com/subscribe/post?u=4e3a46583f7a4c18e57c40d05&id=633de34566" class="validate" id="mc-embedded-subscribe-form" method=
                "post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
                    <div id="mc_embed_signup_scroll">
                        <div class="form-row">
                            <div class="col-lg-4 col-md-12 mt-2">
                                <?php if ( !empty( $instance[ 'title' ] ) ) {
                                    echo $args[ 'before_title' ] .
                                        apply_filters( 'widget_title', $instance[ 'title' ] ) . $args[ 'after_title' ];
                                } ?>

                            </div>
                            <div class="col col-lg-3 col-sm-4">
                                <input class="form-control" id="mce-EMAIL" name="EMAIL" placeholder="Email" type="email" value="">
                            </div>
                            <div class="col col-lg-3 col-sm-4">
                                <input class="form-control" id="mce-MMERGE5" name="MMERGE5" placeholder="ZIP Code" type="text" value="">
                            </div>
                            <div class="col col-lg-2 col-sm-4">
                                <input class="btn btn-primary" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Subscribe">
                            </div>
                        </div>
                        <div class="clear" id="mce-responses">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                        <div aria-hidden="true" style="position: absolute; left: -5000px;">
                            <input name="b_4e3a46583f7a4c18e57c40d05_633de34566" tabindex="-1" type="text" value="">
                        </div>
                    </div>
                </form>
            </div>

            <?php
            wp_enqueue_script( 'mc-validate', 'https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', array(), '1.0.0', true );
            echo $args[ 'after_widget' ];


        }

        //save options
        public function update( $new_instance, $old_instance )
        {
            $instance = [];
            $instance[ 'title' ] = ( !empty( $new_instance[ 'title' ] ) ) ? strip_tags( $new_instance[ 'title' ] ) : '';
            return $new_instance;
        }


    }

    // register My_Widget
    add_action( 'widgets_init', function () {
        register_widget( 'MailchimpSignup' );

    } );
